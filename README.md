# MAGMa Dockerfile

This repository contains a Dockerfile of [MAGMa+](https://github.com/savantas/MAGMa-plus) for an automated build on [Docker Hub](https://hub.docker.com/r/ssmehta/magma/).  MAGMa+ is a wrapper around [MAGMa](https://github.com/NLeSC/MAGMa) (MS Annotation bsed on in-silico Generated Metabolites), and allows for MAGMa metabolite identification with dynamic parameter selection.

## Usage

To run a container in interactive mode

    docker run -it ssmehta/magma-plus

This will run a container with the most recent revision from the MAGMa+ GitHub repository based on the latest [MAGMa](https://hub.docker.com/r/ssmehta/magma/) build.  Two versions of the image exist and are tagged as `v1.0.0` and `v1.0.1`, corresponding to the stable MAGMa+ releases.

This `v1.0.0` build, based on a specific MAGMa build, was used in Blaženović et al. (In Preparation) and is provided to allow for reproducibility of results.

It is possible to mount a host directory or attach data volume to the container's `/data` directory.  The entrypoint script is set up to look for and automatically execute a script at `/data/run.sh` it if it exists.  

## Building

Build with the usual

    docker build -t magma-plus .

and then run with

    docker run -it magma-plus
